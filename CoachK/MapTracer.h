//
//  MapTracer.h
//  Duke
//
//  Created by onegray on 1/18/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ObjectFactory.h"

#define DEBUG_TRACER 1

@class CCNode;

@interface AMapTracer : ObjectFactory 
{

#if DEBUG_TRACER
	CCNode* debugNode;
#endif
	
}

-(CGPoint) traceDirection:(CGPoint)dirVector startPoint:(CGPoint)startPos width:(CGFloat)width;

#if DEBUG_TRACER
@property (nonatomic, readonly) CCNode* debugNode;
#endif
@end


@class CCTMXLayer;

@interface TMXMapTracer : AMapTracer 
{
	CCTMXLayer* tmxLayer;
}
@end


@interface RectMapTracer : AMapTracer
{
	CGRect mapRect;
}
@end