//
//  MapTracer.m
//  Duke
//
//  Created by onegray on 1/18/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "MapTracer.h"
#import "cocos2d.h"
#import "RenderTextureData.h"


#define trim(x,a,b) ( (x)<(a) ? (a) : ((x)>(b) ? (b) : (x)) ) 

@implementation AMapTracer

#if DEBUG_TRACER
@synthesize debugNode;
#endif

-(CGPoint) traceDirection:(CGPoint)dirVector startPoint:(CGPoint)startPos width:(CGFloat)width
{
	return CGPointZero;
}

@end


#if DEBUG_TRACER
@interface TraceDebugNode : CCNode
{
@public
	
	NSMutableArray* rects;
	CGPoint lp0;
	CGPoint lp1;
	CGPoint rp0;
	CGPoint rp1;
}
-(void) addRect:(CGRect)r;
-(void) clear;
@end

@implementation TraceDebugNode

-(id)init
{
	if(self=[super init])
	{
		rects = [[NSMutableArray alloc] init];
	}
	return self;
}

-(void)dealloc
{
	[rects release];
	[super dealloc];
}

-(void) addRect:(CGRect)r
{
	[rects addObject: NSStringFromCGRect(r)];
}

-(void) clear
{
	[rects removeAllObjects];
	lp0 = lp1 = rp0 = rp1 = CGPointZero;
}

-(void) draw
{
//	glColor4f(50, 250, 50, 50);
//	glLineWidth(1);
	
	ccDrawLine(lp0, lp1);
	ccDrawLine(rp0, rp1);
	ccDrawLine(ccpMidpoint(lp0, rp0), ccpMidpoint(lp1, rp1));
	
	ccDrawCircle(ccpMidpoint(lp1, rp1), ccpDistance(lp1, rp1)/2, 0, 20, NO);
	
	for (NSString* strRect in rects )
	{
		CGRect rect = CGRectFromString(strRect);
		CGPoint vertices[4];
		vertices[0] = rect.origin;
		vertices[1] = ccp(rect.origin.x, rect.origin.y+rect.size.height);
		vertices[2] = ccp(rect.origin.x + rect.size.width, rect.origin.y+rect.size.height);
		vertices[3] = ccp(rect.origin.x + rect.size.width, rect.origin.y);
		ccDrawPoly(vertices, 4, YES);
	}
	
	/*
	 for (int i=0; i<10; i++) 
	 for (int j=0; j<15; j++) 
	 if( ![scene tileIsEmpty:ccp(j,i)] )
	 {
	 CGRect rect = CGRectMake(j*32, 320-i*32 -32, 32, 32);
	 
	 CGPoint vertices[4];
	 vertices[0] = rect.origin;
	 vertices[1] = ccp(rect.origin.x, rect.origin.y+rect.size.height);
	 vertices[2] = ccp(rect.origin.x + rect.size.width, rect.origin.y+rect.size.height);
	 vertices[3] = ccp(rect.origin.x + rect.size.width, rect.origin.y);
	 ccDrawPoly(vertices, 4, YES);
	 }
	 */
}

@end
#endif



@implementation TMXMapTracer

+(id) objectWithProperties: (NSDictionary*) properties
{
	return [[[TMXMapTracer alloc] initWithProperties:properties] autorelease];
}

-(id) initWithProperties:(NSDictionary*)properties
{
	if( self = [super init] )
	{
		NSString* tmxFileName = [properties objectForKey:@"tmx"];
		CCTMXTiledMap* tmx = [CCTMXTiledMap tiledMapWithTMXFile:tmxFileName];

		NSString* tmxLayerName = [properties objectForKey:@"tmxlayer"];
		if(tmxLayerName!=nil)
		{
			tmxLayer = [[tmx layerNamed:tmxLayerName] retain];
		}
		else
		{
			tmxLayer = [[[tmx children]	objectAtIndex:0] retain];
		}
		
#if DEBUG_TRACER
		debugNode = [[TraceDebugNode node] retain];
#endif
		
	}
	return self;
}

-(void)dealloc
{
#if DEBUG_TRACER
	[debugNode release];
#endif
	[tmxLayer release];
	[super dealloc];
}



-(BOOL) tileIsEmpty:(CGPoint) tile
{
	CGSize mapSize = tmxLayer.layerSize;
	int tileGID = [tmxLayer tileGIDAt:ccp(tile.x, mapSize.height-1- tile.y)];
	return tileGID==0;
}

-(NSData*)rawDataFromTile:(CGPoint)tile
{
	CGSize mapSize = tmxLayer.layerSize;
	tile = ccp(tile.x, mapSize.height-1- tile.y);
	CCSprite* tileSprite = [tmxLayer tileAt:tile];
	CCTexture2D* tileTexture = [tileSprite texture];
	
	CGRect tileRect = [tileSprite textureRect];
	CCSprite* tileSpriteCopy = [CCSprite spriteWithTexture:tileTexture rect:tileRect];
	tileSpriteCopy.anchorPoint = ccp(0,0); 
	
	CCRenderTexture* renderTexture = [CCRenderTexture renderTextureWithWidth:tileRect.size.width height:tileRect.size.height];
	[renderTexture begin];
	[tileSpriteCopy visit];
	[renderTexture end];
	
	NSData* data = [renderTexture getTextureDataWithRect:CGRectMake(0,0, tileRect.size.width, tileRect.size.height)];
	return data;
}



-(CGPoint) traceTileVertical:(CGPoint)tile dirVector:(CGPoint)dirVector dir0:(CGPoint)dir0 p0:(CGPoint)p0 width:(CGFloat)w
{
	CGSize tsz = tmxLayer.mapTileSize;
	CGRect rect = CGRectMake(0, 0, tsz.width, tsz.height);
	NSData* tileData = [self rawDataFromTile:tile];
	int32_t* bytes = (int32_t*)[tileData bytes];
	
	int signy = dirVector.y > 0 ? 1 : -1;
	
	int x0 = p0.x - tsz.width*tile.x - w;
	x0 = trim(x0,0,tsz.width-1);
	int x1 = p0.x - tsz.width*tile.x + w;
	x1 = trim(x1,0,tsz.width-1);
	
	int y0 = p0.y - tsz.height*tile.y;
	y0 = trim(y0,0,tsz.height-1);
	
	int y1 = signy > 0 ? tsz.height-1 : 0;
	if( (y1-y0)*signy <0 )
		return CGPointZero;
	
	CGPoint minTracePoint;
	CGFloat minTraceDistance = INT_MAX;
	
	for (int y = y0; y!=y1+signy; y=y+signy) 
	{
		int idx =  y*tsz.width;
		for (int x=x0; x<=x1; x++) 
		{
			int32_t val = bytes[idx+x];
			if(val!=0)
			{
				CGPoint hPoint = ccp(p0.x-tsz.width*tile.x, y);
				CGFloat d = (x-hPoint.x);
				CGFloat l = sqrt(w*w - d*d);
				CGPoint tracePoint  = ccp(tsz.width*tile.x + hPoint.x, tsz.height*tile.y + hPoint.y-l*signy);
				
				CGFloat traceDistance = (tracePoint.y - dir0.y)*(tracePoint.y - dir0.y);
				if(traceDistance < minTraceDistance)
				{
					minTraceDistance = traceDistance;
					minTracePoint = tracePoint;
				}
			}
		}
	}
	
	if(minTraceDistance!=INT_MAX)
	{
		return minTracePoint;
	}
	
	return CGPointZero;
}

-(CGPoint) traceTileHorizontal:(CGPoint)tile dirVector:(CGPoint)dirVector dir0:(CGPoint)dir0 p0:(CGPoint)p0 width:(CGFloat)w
{
	CGSize tsz = tmxLayer.mapTileSize;
	CGRect rect = CGRectMake(0, 0, tsz.width, tsz.height);
	NSData* tileData = [self rawDataFromTile:tile];
	int32_t* bytes = (int32_t*)[tileData bytes];

	int signx = dirVector.x > 0 ? 1 : -1;
	
	int y0 = p0.y - tsz.height*tile.y - w;
	y0 = trim(y0, 0, tsz.height-1);
	int y1 = p0.y - tsz.height*tile.y + w;
	y1 = trim(y1, 0, tsz.height-1);
	
	int x0 = p0.x - tsz.width*tile.x;
	x0 = trim(x0,0,tsz.width-1);
	int x1 = signx > 0 ? tsz.width-1 : 0;
	x1 = trim(x1,0,tsz.width-1);
	if( (x1-x0)*signx <0 )
		return CGPointZero;
	
	CGPoint minTracePoint;
	CGFloat minTraceDistance = INT_MAX;
	
	for (int x = x0; x!=x1+signx; x=x+signx) 
	{
		for (int y=y0; y<=y1; y++) 
		{
			int idx =  y*tsz.width +x;
			int32_t val = bytes[idx];
			if(val!=0)
			{
				CGPoint hPoint = ccp(x, p0.y - tsz.height*tile.y);
				CGFloat d = (y-hPoint.y);
				CGFloat l = sqrt(w*w - d*d);
				CGPoint tracePoint  = ccp(tsz.width*tile.x + hPoint.x -l*signx, tsz.height*tile.y + hPoint.y);
				
				CGFloat traceDistance = (tracePoint.x - dir0.x)*(tracePoint.x - dir0.x);
				if(traceDistance < minTraceDistance)
				{
					minTraceDistance = traceDistance;
					minTracePoint = tracePoint;
				}
			}
		}
	}
	
	if(minTraceDistance!=INT_MAX)
	{
		return minTracePoint;
	}
	
	
	return CGPointZero;
}

-(CGPoint) traceDirectionVertical:(CGPoint)dirVector startPoint:(CGPoint)startPos width:(CGFloat)width
{
	CGSize tsz = tmxLayer.mapTileSize;
	CGSize msz = tmxLayer.layerSize;
	int signy = dirVector.y > 0 ? 1 : -1;
	
	int tx0 = (startPos.x - width/2)/ tsz.width;
	tx0 = tx0 < 0 ? 0 : tx0;
	int tx1 = (startPos.x + width/2)/ tsz.width;
	tx1 = tx1 > msz.width-1 ? msz.width-1 : tx1;
	
	CGPoint minTracePoint;
	CGFloat minTraceDistance = INT_MAX;
	int yfound = 2;
	
	
	int ty = startPos.y / tsz.height;
	while (ty>=0 && ty<msz.height && yfound>0) 
	{
		for(int tx = tx0; tx<=tx1; tx++) 
		{
#if DEBUG_TRACER
			[(TraceDebugNode*)debugNode addRect:CGRectMake(tx*tsz.width, ty*tsz.height, tsz.width, tsz.height)];
#endif
			if( ![self tileIsEmpty:ccp(tx, ty)] )
			{
#if DEBUG_TRACER
				[(TraceDebugNode*)debugNode addRect:CGRectMake(tx*tsz.width+2, (ty*tsz.height+2), tsz.width-4, tsz.height-4)];
#endif
				CGPoint tracePoint = CGPointZero;
				int x = startPos.x;
				int y = ty*tsz.height + ( signy<0 ? tsz.height : 0 );
				
				tracePoint = [self traceTileVertical:ccp(tx, ty) dirVector:dirVector dir0:startPos p0:ccp(x, y) width:width/2];
				
				if(tracePoint.x!=0 && tracePoint.y!=0)
				{
					CGFloat distance = ccpLengthSQ(ccpSub(tracePoint, startPos));
					if(distance < minTraceDistance)
					{
						minTraceDistance = distance;
						minTracePoint = tracePoint;
					}
				}
			}
		}
		
		if(minTraceDistance!=INT_MAX)
		{
			yfound--;
		}
		
		ty = ty + signy;
	}
	
	if(minTraceDistance!=INT_MAX)
	{
#if DEBUG_TRACER		
		((TraceDebugNode*)debugNode)->lp0 = ccp( startPos.x-width/2, startPos.y);
		((TraceDebugNode*)debugNode)->rp0 = ccp( startPos.x+width/2, startPos.y);
		((TraceDebugNode*)debugNode)->lp1 = ccp( minTracePoint.x-width/2, minTracePoint.y);
		((TraceDebugNode*)debugNode)->rp1 = ccp( minTracePoint.x+width/2, minTracePoint.y);
#endif
		return minTracePoint;
	}
	
	return CGPointZero;
}


-(CGPoint) traceDirectionHorizontal:(CGPoint)dirVector startPoint:(CGPoint)startPos width:(CGFloat)width
{
	CGSize tsz = tmxLayer.mapTileSize;
	CGSize msz = tmxLayer.layerSize;
	int signx = dirVector.x > 0 ? 1 : -1;
	
	int ty0 = (startPos.y - width/2)/ tsz.height;
	ty0 = ty0 < 0 ? 0 : ty0;
	int ty1 = (startPos.y + width/2)/ tsz.height;
	ty1 = ty1 > msz.height-1 ? msz.height-1 : ty1;
	
	CGPoint minTracePoint;
	CGFloat minTraceDistance = INT_MAX;
	int xfound = 2;
	
	int tx = startPos.x / tsz.height;
	while (tx>=0 && tx<msz.width && xfound>0) 
	{
		for(int ty = ty0; ty<=ty1; ty++) 
		{
#if DEBUG_TRACER
			[(TraceDebugNode*)debugNode addRect:CGRectMake(tx*tsz.width, ty*tsz.height, tsz.width, tsz.height)];
#endif
			if( ![self tileIsEmpty:ccp(tx, ty)] )
			{
#if DEBUG_TRACER
				[(TraceDebugNode*)debugNode addRect:CGRectMake(tx*tsz.width+2, (ty*tsz.height+2), tsz.width-4, tsz.height-4)];
#endif
				CGPoint tracePoint = CGPointZero;
				int y = startPos.y;
				int x = tx*tsz.width + ( signx<0 ? tsz.width : 0 );
				
				tracePoint = [self traceTileHorizontal:ccp(tx, ty) dirVector:dirVector dir0:startPos p0:ccp(x, y) width:width/2];
				
				if(tracePoint.x!=0 && tracePoint.y!=0)
				{
					CGFloat distance = (tracePoint.x-startPos.x)*(tracePoint.x-startPos.x);
					if(distance < minTraceDistance)
					{
						minTraceDistance = distance;
						minTracePoint = tracePoint;
					}
				}
			}
		}
		
		if(minTraceDistance!=INT_MAX)
		{
			xfound--;
		}
		
		tx= tx + signx;
	}
	
	if(minTraceDistance!=INT_MAX)
	{
#if DEBUG_TRACER		
		((TraceDebugNode*)debugNode)->lp0 = ccp( startPos.x, startPos.y+width/2);
		((TraceDebugNode*)debugNode)->rp0 = ccp( startPos.x, startPos.y-width/2);
		((TraceDebugNode*)debugNode)->lp1 = ccp( minTracePoint.x, minTracePoint.y+width/2);
		((TraceDebugNode*)debugNode)->rp1 = ccp( minTracePoint.x, minTracePoint.y-width/2);
#endif
		return minTracePoint;
	}
	
	return CGPointZero;
}


-(CGPoint) traceTile:(CGPoint)tile dirVector:(CGPoint)dirVector dir0:(CGPoint)dir0 p0:(CGPoint)p0 width:(CGFloat)w wx:(CGFloat)wx
{
	CGFloat dx = dirVector.x/dirVector.y;
	
	int signx = dirVector.x > 0 ? 1 : -1;
	int signy = dirVector.y > 0 ? 1 : -1;
	
	CGSize tsz = tmxLayer.mapTileSize;
	CGRect rect = CGRectMake(0, 0, tsz.width, tsz.height);
	NSData* tileData = [self rawDataFromTile:tile];
	int32_t* bytes = (int32_t*)[tileData bytes];
	
	CGFloat x0 = p0.x - tsz.width*tile.x;
	int y0 = p0.y - tsz.height*tile.y;
	int y1 = signy > 0 ? tsz.height-1 : 0;
	
	CGFloat A = dirVector.y / dirVector.x;
	CGFloat B = y0 - A*x0;
	CGFloat C = dirVector.x / dirVector.y;
	
	CGFloat _x0 = x0;
	
	CGPoint minTracePoint;
	CGFloat minTraceDistance = INT_MAX;
	
	if( (y1-y0)*signy <0 )
		return CGPointZero;
		
	for (int y=y0; y!=y1+signy; y=y+signy)
	{
		int idx =  y*tsz.width;
		for (int x = x0-wx; x<=x0+wx; x++  )
		{
			if(x>=0 && x <tsz.width)
			{
				int32_t val = bytes[idx+x];
				if(val!=0)
				{
					CGPoint hPoint;
					hPoint.x = (A*_x0+C*x+y-y0)/(A+C);
					hPoint.y = A*hPoint.x + B;
					CGPoint tracePoint  = ccp(tsz.width*tile.x + hPoint.x, tsz.height*tile.y + hPoint.y);
					
					if( (tracePoint.x - dir0.x)*dirVector.x >0 && (tracePoint.y - dir0.y)*dirVector.y >0 )
					{
						
						CGFloat d2 = ccpLengthSQ(ccp(x-hPoint.x, y-hPoint.y));
						CGFloat l = sqrt(w*w - d2);
						CGPoint lv = ccpMult(ccpNormalize(dirVector), l);
						tracePoint = ccpSub(hPoint, lv);
						tracePoint  = ccp(tsz.width*tile.x + tracePoint.x, tsz.height*tile.y + tracePoint.y);
						
						CGFloat traceDistance = ccpLengthSQ(ccpSub(tracePoint,dir0));
						if(traceDistance < minTraceDistance)
						{
							minTraceDistance = traceDistance;
							minTracePoint = tracePoint;
						}
					}
				}
			}
		}
		x0 = x0+signy*dx;
	}
	
	if(minTraceDistance!=INT_MAX)
	{
		return minTracePoint;
	}
	
	return CGPointZero;
}


-(CGPoint) traceDirection:(CGPoint)dirVector startPoint:(CGPoint)startPos width:(CGFloat)width
{
#if DEBUG_TRACER
	[(TraceDebugNode*)debugNode clear];
#endif
	
	if(dirVector.y != 0 && dirVector.x == 0)
	{
		return [self traceDirectionVertical:dirVector startPoint:startPos width:width];
	}
	else if(dirVector.y == 0 && dirVector.x != 0)
	{
		return [self traceDirectionHorizontal:dirVector startPoint:startPos width:width];
	}
	else if(dirVector.y == 0 && dirVector.x == 0)
	{
		return CGPointZero;
	}
	
	CGSize tsz = tmxLayer.mapTileSize;
	CGSize msz = tmxLayer.layerSize;
	
	
	CGPoint wVec = ccpMult(dirVector, (width/2)/ccpLength(dirVector));
	CGPoint lp = ccpAdd(startPos, ccpPerp(wVec));
	CGPoint rp = ccpAdd(startPos, ccpRPerp(wVec));
	
#if DEBUG_TRACER
	((TraceDebugNode*)debugNode)->lp0 = lp;
	((TraceDebugNode*)debugNode)->lp1 = ccpAdd(lp, ccpMult(dirVector,100));
	((TraceDebugNode*)debugNode)->rp0 = rp;
	((TraceDebugNode*)debugNode)->rp1 = ccpAdd(rp, ccpMult(dirVector,100));
#endif
	
	// line func: y = A*x + B
	CGFloat A = dirVector.y / dirVector.x;
	CGFloat B = startPos.y - A*startPos.x;
	
	// line func: x = C*y + D
	CGFloat C = dirVector.x / dirVector.y;
	CGFloat D = startPos.x - C*startPos.y;
	
	CGFloat wx = wVec.y + C*wVec.x;
	wx = wx > 0 ? wx : -wx;
	CGFloat wy = A * wx;
	wy = wy > 0 ? wy : -wy;
	
	int signx = dirVector.x > 0 ? 1 : -1;
	int signy = dirVector.y > 0 ? 1 : -1;
	
	int tx0 = (signx*lp.x < signx*rp.x ? lp.x : rp.x) / tsz.width;
	int ty0 = (signy*lp.y < signy*rp.y ? lp.y : rp.y) / tsz.height;
	
	int _tx0 = tx0;
	int _ty0 = ty0;
	
	CGPoint minTracePoint;
	CGFloat minTraceDistance = INT_MAX;
	int xfound = 2;
	
	while( tx0>=0 && tx0<msz.width && xfound>0 )
	{
		int x1 = tx0*tsz.width + (signx>0 ? tsz.width : 0);
		int y1 = A*x1 + B;
		
		int ty1 = (y1 + signy*wy) / tsz.height;
		
		for(int ty = ty0; ty!=ty1+signy; ty+=signy ) 
		{
			if( ty < 0 || ty >= msz.height )
				break;
			
#if DEBUG_TRACER
			[(TraceDebugNode*)debugNode addRect:CGRectMake(tx0*tsz.width, ty*tsz.height, tsz.width, tsz.height)];
#endif
			if( ![self tileIsEmpty:ccp(tx0, ty)] )
			{
#if DEBUG_TRACER
				[(TraceDebugNode*)debugNode addRect:CGRectMake(tx0*tsz.width+2, (ty*tsz.height+2), tsz.width-4, tsz.height-4)];
#endif
				CGPoint tracePoint = CGPointZero;

				CGFloat y = ty*tsz.height + ( signy<0 ? tsz.height : 0 );
				
				if( y*signy < startPos.y*signy )
				{
					y = startPos.y;
				}
				CGFloat x = C*y + D;
				
				tracePoint = [self traceTile:ccp(tx0, ty) dirVector:dirVector dir0:startPos p0:ccp(x, y) width:width/2 wx:wx];
	
				if(tracePoint.x!=0 && tracePoint.y!=0)
				{
					CGFloat distance = ccpLengthSQ(ccpSub(tracePoint, startPos));
					if(distance < minTraceDistance)
					{
						minTraceDistance = distance;
						minTracePoint = tracePoint;
					}
					break;					
				}
			}			
		}
		
		if(minTraceDistance!=INT_MAX)
		{
			xfound--;
		}
		
		tx0 = tx0 + signx;
		ty0 = (y1 - signy*wy ) / tsz.height;
		if( signy*ty0 < signy*_ty0 )
			ty0 = _ty0;
	}
	
	if(minTraceDistance!=INT_MAX)
	{
#if DEBUG_TRACER		
		((TraceDebugNode*)debugNode)->lp1 = ccpAdd( minTracePoint, ccpPerp(wVec));
		((TraceDebugNode*)debugNode)->rp1 = ccpAdd( minTracePoint, ccpRPerp(wVec));
#endif
		return minTracePoint;
	}
	
	return CGPointZero;
}



@end





@implementation RectMapTracer

+(id) objectWithProperties: (NSDictionary*) properties
{
	return [[[RectMapTracer alloc] initWithProperties:properties] autorelease];
}

-(id) initWithProperties:(NSDictionary*)properties
{
	if( self = [super init] )
	{
		NSString* rectStr = [properties objectForKey:@"rect"];
		mapRect = CGRectFromString(rectStr);
		
#if DEBUG_TRACER
		debugNode = [[TraceDebugNode node] retain];
		[((TraceDebugNode*)debugNode) addRect:mapRect];
#endif
		
	}
	return self;
}

-(void)dealloc
{
#if DEBUG_TRACER
	[debugNode release];
#endif
	[super dealloc];
}


-(CGPoint) traceDirection:(CGPoint)dirVector inRect:(CGRect)rect startPoint:(CGPoint)startPos
{
	if( !CGRectContainsPoint(rect, startPos) )
	{
		return startPos;
	}

	if(dirVector.y == 0 && dirVector.x == 0)
	{
		return startPos;
	}
	else if(dirVector.y != 0 && dirVector.x == 0)
	{
		return ccp(startPos.x, dirVector.y < 0 ? rect.origin.y : rect.origin.y + rect.size.height);
	}
	else if(dirVector.y == 0 && dirVector.x != 0)
	{
		return ccp(dirVector.x < 0 ? rect.origin.x : rect.origin.x + rect.size.width, startPos.y);
	}

	// line func: y = A*x + B
	CGFloat A = dirVector.y / dirVector.x;
	CGFloat B = startPos.y - A*startPos.x;
	
	// line func: x = C*y + D
	CGFloat C = dirVector.x / dirVector.y;
	CGFloat D = startPos.x - C*startPos.y;

	CGFloat y =  dirVector.y < 0 ? rect.origin.y : rect.origin.y + rect.size.height;
	CGFloat x = C*y + D;
	
	if( x < rect.origin.x )
	{
		x = rect.origin.x;
		y = A*x + B;
	}
	else if ( x > rect.origin.x+rect.size.width)
	{
		x = rect.origin.x+rect.size.width;
		y = A*x + B;
	}		
	
	return ccp(x,y);
}

-(CGPoint) traceDirection:(CGPoint)dirVector startPoint:(CGPoint)startPos width:(CGFloat)width
{
#if DEBUG_TRACER
	[(TraceDebugNode*)debugNode clear];
	[(TraceDebugNode*)debugNode addRect:mapRect];
#endif
	
	CGRect rect = CGRectInset(mapRect, width/2, width/2);
	if( !CGRectContainsPoint(rect, startPos) )
	{
		return startPos;
	}

	CGPoint tp = [self traceDirection:dirVector inRect:rect startPoint:startPos];
	
#if DEBUG_TRACER
	CGPoint wVec = ccpMult(dirVector, (width/2)/ccpLength(dirVector));
	((TraceDebugNode*)debugNode)->lp0 = ccpAdd(startPos, ccpPerp(wVec));
	((TraceDebugNode*)debugNode)->lp1 = ccpAdd(tp, ccpPerp(wVec));
	((TraceDebugNode*)debugNode)->rp0 = ccpAdd(startPos, ccpRPerp(wVec));
	((TraceDebugNode*)debugNode)->rp1 = ccpAdd(tp, ccpRPerp(wVec));
#endif	
	
	return tp;
}

@end

