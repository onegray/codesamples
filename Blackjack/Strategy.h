//
//  Strategy.h
//  Blackjack
//
//  Created by onegray on 11/22/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Player.h"
#import "Hand.h"

@interface Strategy : NSObject 
{
}

-(ActionType) actionForHand:(Hand*)hand dealerCard:(Card*)dealerCard;

@end


@interface DefaultStrategy:Strategy
{
}
@end;