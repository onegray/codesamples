//
//  Strategy.m
//  Blackjack
//
//  Created by onegray on 11/22/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Strategy.h"
#import "Card.h"

@implementation Strategy

-(ActionType) actionForHand:(Hand*)hand dealerCard:(Card*)dealerCard
{
	return AT_NO_ACTION;
}

@end



@implementation DefaultStrategy

#define H	AT_HIT
#define S	AT_STAND
#define D	AT_DOUBLE
#define Sp	AT_SPLIT
#define Su	AT_SURRENDER
#define A	CR_ACE

typedef struct sStrategyTableRowType
{
	int cards[2];
	ActionType actions[10];
} strategyTableRowType;

static const strategyTableRowType strategyTable[] = 
{
	{ {  8 }, { H, H, H, H, H, H, H, H, H, H, } },
	{ {  9 }, { H, D, D, D, D, H, H, H, H, H, } },
	{ { 10 }, { D, D, D, D, D, D, D, D, H, H, } },
	{ { 11 }, { D, D, D, D, D, D, D, D, D, D, } },
	{ { 12 }, { H, H, S, S, S, H, H, H, H, H, } },
	{ { 13 }, { S, S, S, S, S, H, H, H, H, H, } },
	{ { 14 }, { S, S, S, S, S, H, H, H, H, H, } },
	{ { 15 }, { S, S, S, S, S, H, H, H, Su,Su,} },
	{ { 16 }, { S, S, S, S, S, H, H, Su,Su,Su,} },
	{ { 17 }, { S, S, S, S, S, S, S, S, S, Su,} },
	
	{ { A, 2 }, { H, H, H, D, D, H, H, H, H, H, } },
	{ { A, 3 }, { H, H, H, D, D, H, H, H, H, H, } },
	{ { A, 4 }, { H, H, D, D, D, H, H, H, H, H, } },
	{ { A, 5 }, { H, H, D, D, D, H, H, H, H, H, } },
	{ { A, 6 }, { H, D, D, D, D, H, H, H, H, H, } },
	{ { A, 7 }, { D, D, D, D, D, S, S, H, H, H, } },
	{ { A, 8 }, { S, S, S, S, D, S, S, S, S, S, } },
	
	{ { 2, 2 }, { Sp,Sp,Sp,Sp,Sp,Sp,H, H, H, H, } },
	{ { 3, 3 }, { Sp,Sp,Sp,Sp,Sp,Sp,H, H, H, H, } },
	{ { 4, 4 }, { H, H, H, Sp,Sp,H, H, H, H, H, } },
	{ { 5, 5 }, { D, D, D, D, D, D, D, D, H, H, } },
	{ { 6, 6 }, { Sp,Sp,Sp,Sp,Sp,H, H, H, H, H, } },
	{ { 7, 7 }, { Sp,Sp,Sp,Sp,Sp,Sp,H, H, H, H, } },
	{ { 8, 8 }, { Sp,Sp,Sp,Sp,Sp,Sp,Sp,Sp,Sp,H, } },
	{ { 9, 9 }, { Sp,Sp,Sp,Sp,Sp,S, Sp,Sp,S, S, } },
	{ {10,10 }, { S, S, S, S, S, S, S, S, S, S, } },
	{ { A, A }, { Sp,Sp,Sp,Sp,Sp,Sp,Sp,Sp,Sp,Sp,} },
};

static const int strategyTableSize = sizeof(strategyTable)/sizeof(strategyTable[0]);
static const int actionsSize = sizeof(strategyTable[0].actions)/sizeof(strategyTable[0].actions[0]);

-(ActionType) actionForHand:(Hand*)hand dealerCard:(Card*)dealerCard
{
	ActionType action = AT_NO_ACTION;
	
	int dealerCardIndex = 0;
	
	CardRankType dealerCardRank = [dealerCard cardRank];
	if (dealerCardRank >= CR_TWO && dealerCardRank<=CR_TEN)
	{
		dealerCardIndex = dealerCardRank - CR_TWO;
	}
	else if (dealerCardRank>CR_TEN && dealerCardRank<CR_ACE)
	{
		dealerCardIndex = CR_TEN - CR_TWO;
	}
	else if (dealerCardRank==CR_ACE)
	{
		dealerCardIndex = CR_TEN - CR_TWO + 1;
	}
	
	if( [hand.cards count]>=2 && dealerCardIndex>=0 && dealerCardIndex<actionsSize )
	{
		if([hand.cards count]==2)
		{
			CardRankType card1 = [[hand.cards objectAtIndex:0] cardRank];
			CardRankType card2 = [[hand.cards objectAtIndex:1] cardRank];
			
			if(card2==CR_ACE && card1!=CR_ACE)
			{
				card2 = card1;
				card1 = CR_ACE;
			}
			
			for(int i=0; i<strategyTableSize; i++)
			{
				if( strategyTable[i].cards[0]==card1 && strategyTable[i].cards[1]==card2 )
				{
					action = strategyTable[i].actions[dealerCardIndex];
					break;
				}
			}
		}
		
		if(action==AT_NO_ACTION)
		{
			int handRank = [hand handRank];
			for(int i=0; i<strategyTableSize; i++)
			{
				if( strategyTable[i].cards[0]==handRank &&  strategyTable[i].cards[1]==0 )
				{
					action = strategyTable[i].actions[dealerCardIndex];
					break;
				}
			}
		}
	}
	
	if(action==AT_NO_ACTION )
	{
		NSLog(@"Not found strategy action for hand:\n%@ \n dealerCard:%@",
			  [hand description], [dealerCard cardStringValue]);
	}

	return action;
}

@end
