
# Coach K iPhone App

http://coachk.com/coach-k-news/coach-k-launches-new-iphone-game-app/  
http://blog.dukeblueplanet.com/2010/03/coach-ks-new-iphone-app/  

#### Code Sample

[CoachK/](https://bitbucket.org/onegray/codesamples/raw/master/CoachK/)

#### Description

`MapTracer` is a class that encapsulates algorithm to track obstacles on a tiled map. Obstacles are placed on the separate layer of the map.

#### Algorithm

A 'ray' abstraction is used to find obstacles in the given direction. It needs to check all tiles on the the way of the test ray. If the certain tile is not empty, then an additional checking is performed to test for intersection with tile image (alpha channel is used as a mask).

![Demo](https://bitbucket.org/onegray/codesamples/raw/master/CoachK/obstacles_tracking_demo.png)

  
  
# Lomo Filter iOS implementation

The challenge was to research and implement Lomo Filter algorithm described in the article:  
http://digital-photography-school.com/how-to-make-digital-photos-look-like-lomo-photography/

#### Code Sample

[LomoFilter/](https://bitbucket.org/onegray/codesamples/raw/master/LomoFilter/)

  
  
# Fantasy Blackjack app
Fantasy Blackjack game sponsored by casino Palms  
http://www.youtube.com/watch?v=6uYihRzbMCg  

#### Code Sample

[Blackjack/](https://bitbucket.org/onegray/codesamples/raw/master/Blackjack/)

#### Description

`DefaultStrategy` is a class that encapsulates AI strategy for playing Blackjack game.  



# Personal open source projects

https://github.com/onegray



